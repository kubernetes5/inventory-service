package com.ansp.inventoryservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND) 
public class ProductTypeNotFoundException extends RuntimeException {
	public ProductTypeNotFoundException(String message) {
		super(message);
	}

}
