package com.ansp.inventoryservice.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.ansp.inventoryservice.service.ProductService;
import com.ansp.inventoryservice.entity.*;
@Api
@RestController
public class ProductController {
	@Autowired
	private ProductService service;
	@ApiOperation("Retorna los productos")
	@GetMapping("/product")
	public List<Product> findAll() {
		return this.service.findAll();
	}
	@ApiOperation("Guarda un producto")
	@PostMapping("/product")
	public Product save(@Valid @RequestBody Product product) {
		return this.service.save(product);
	}
	@ApiOperation("Actualiza un producto")
	@PutMapping("/product/{id}")
	public Product update(@RequestBody Product product, @PathVariable("id") Integer id) {
		product.setId(id);
		return this.service.update(product);
	}
	@ApiOperation("Retorna un producto")
	@GetMapping("/product/{id}")
	public Product findById(@PathVariable("id") Integer id) throws Exception {
		return this.service.findById(id);
	}
	@ApiOperation("Borra un producto")
	@DeleteMapping("/product/{id}")
	public void delete(@PathVariable("id") Integer id) {
		this.service.deleteById(id);
	}
}
