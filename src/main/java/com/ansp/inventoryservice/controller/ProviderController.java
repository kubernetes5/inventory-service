package com.ansp.inventoryservice.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ansp.inventoryservice.entity.Provider;
import com.ansp.inventoryservice.service.ProviderService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
@Api
@RestController
public class ProviderController {
	@Autowired
	private ProviderService service;
	@ApiOperation("Retorna los provider")
	@GetMapping("/provider")
	public List<Provider> findAll() {
		return this.service.findAll();
	}
	@ApiOperation("Guarda un producto")
	@PostMapping("/provider")
	public Provider save(@Valid @RequestBody Provider provider) {
		return this.service.save(provider);
	}
	@ApiOperation("Actualiza un producto")
	@PutMapping("/provider/{id}")
	public Provider update(@RequestBody Provider provider, @PathVariable("id") Integer id) {
		provider.setId(id);
		return this.service.update(provider);
	}
	@ApiOperation("Retorna un producto")
	@GetMapping("/provider/{id}")
	public Provider findById(@PathVariable("id") Integer id) throws Exception {
		return this.service.findById(id);
	}
	@ApiOperation("Borra un producto")
	@DeleteMapping("/provider/{id}")
	public void delete(@PathVariable("id") Integer id) {
		this.service.deleteById(id);
	}
}
