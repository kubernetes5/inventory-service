package com.ansp.inventoryservice.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ansp.inventoryservice.entity.ProductType;
import com.ansp.inventoryservice.service.ProductTypeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
@Api
@RestController
public class ProductTypeController {
	@Autowired
	private ProductTypeService service;
	@ApiOperation("Retorna los productos")
	@GetMapping("/producttype")
	public List<ProductType> findAll() {
		return this.service.findAll();
	}
	@ApiOperation("Guarda un producto")
	@PostMapping("/producttype")
	public ProductType save(@Valid @RequestBody ProductType producttype) {
		return this.service.save(producttype);
	}
	@ApiOperation("Actualiza un producto")
	@PutMapping("/producttype/{id}")
	public ProductType update(@RequestBody ProductType producttype, @PathVariable("id") Integer id) {
		producttype.setId(id);
		return this.service.update(producttype);
	}
	@ApiOperation("Retorna un producto")
	@GetMapping("/producttype/{id}")
	public ProductType findById(@PathVariable("id") Integer id) throws Exception {
		return this.service.findById(id);
	}
	@ApiOperation("Borra un producto")
	@DeleteMapping("/producttype/{id}")
	public void delete(@PathVariable("id") Integer id) {
		this.service.deleteById(id);
	}
}
