package com.ansp.inventoryservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ansp.inventoryservice.entity.ProductType;

public interface ProductTypeRepository extends JpaRepository<ProductType,Integer>{

}
