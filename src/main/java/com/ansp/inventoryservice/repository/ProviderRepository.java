package com.ansp.inventoryservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ansp.inventoryservice.entity.Provider;

public interface ProviderRepository extends JpaRepository<Provider,Integer>{

}
