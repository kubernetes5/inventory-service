package com.ansp.inventoryservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ansp.inventoryservice.entity.Product;

public interface ProductRepository extends JpaRepository<Product,Integer>

{

}
