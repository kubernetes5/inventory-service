package com.ansp.inventoryservice.service;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ansp.inventoryservice.entity.ProductType;
import com.ansp.inventoryservice.exception.ProductTypeNotFoundException;
import com.ansp.inventoryservice.repository.ProductTypeRepository;
@Service
public class ProductTypeService {
	@Autowired
	private ProductTypeRepository repository;
	
	public List<ProductType> findAll(){
		
		return (List<ProductType>) this.repository.findAll();
	}
	
	public ProductType save(ProductType producttype) {
		return this.repository.save(producttype);
		
	}
	public ProductType update(ProductType producttype) {
		return this.repository.save(producttype);
		
	}
	public void deleteById(Integer id) {
		this.repository.deleteById(id);
		
	}

	public ProductType findById(Integer id) {
		// TODO Auto-generated method stub
		return this.repository.findById(id).orElseThrow(()->new ProductTypeNotFoundException("Producto No encontrado"));
	}
	
}
