package com.ansp.inventoryservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ansp.inventoryservice.entity.Provider;
import com.ansp.inventoryservice.exception.ProviderNotFoundException;
import com.ansp.inventoryservice.repository.ProviderRepository;
@Service
public class ProviderService {
	@Autowired
	private ProviderRepository repository;
	
	public List<Provider> findAll(){
		
		return (List<Provider>) this.repository.findAll();
	}
	
	public Provider save(Provider provider) {
		return this.repository.save(provider);
		
	}
	public Provider update(Provider provider) {
		return this.repository.save(provider);
		
	}
	public void deleteById(Integer id) {
		this.repository.deleteById(id);
		
	}

	public Provider findById(Integer id) {
		// TODO Auto-generated method stub
		return this.repository.findById(id).orElseThrow(()->new ProviderNotFoundException("Producto No encontrado"));
	}
}
