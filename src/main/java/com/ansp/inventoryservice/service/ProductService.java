package com.ansp.inventoryservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ansp.inventoryservice.entity.Product;
import com.ansp.inventoryservice.exception.ProductNotFoundException;
import com.ansp.inventoryservice.repository.ProductRepository;

@Service
public class ProductService  {

	@Autowired
	private ProductRepository repository;
	
	public List<Product> findAll(){
		
		return (List<Product>) this.repository.findAll();
	}
	
	public Product save(Product product) {
		return this.repository.save(product);
		
	}
	public Product update(Product product) {
		return this.repository.save(product);
		
	}
	public void deleteById(Integer id) {
		this.repository.deleteById(id);
		
	}

	public Product findById(Integer id) {
		// TODO Auto-generated method stub
		return this.repository.findById(id).orElseThrow(()->new ProductNotFoundException("Producto No encontrado"));
	}
	
}
